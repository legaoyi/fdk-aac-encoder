# fdk-aac-encoder

MP3，wav转aac

fork https://github.com/sheinbergon/jna-aac-encoder.git

如有侵权，麻烦联系admin@timmyfun.com

引入方法
``````
<repositories>
    <repository>
        <id>jitpack.io</id>
        <url>https://jitpack.io</url>
    </repository>
</repositories>
<dependency>
    <groupId>com.gitee.gdty</groupId>
    <artifactId>fdk-aac-encoder</artifactId>
    <version>1.0.0</version>
</dependency>
``````

使用方法
``````
AudioInputStream input = AudioSystem.getAudioInputStream(...);
File output = new File(...);
AudioSystem.write(input, AACFileTypes.AAC_LC, output);
``````