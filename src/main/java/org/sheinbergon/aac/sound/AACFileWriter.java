package org.sheinbergon.aac.sound;

import lombok.val;
import org.sheinbergon.aac.encoder.AACAudioEncoder;
import org.sheinbergon.aac.encoder.AACAudioOutput;
import org.sheinbergon.aac.encoder.WAVAudioInput;
import org.sheinbergon.aac.encoder.util.AACEncodingProfile;
import org.sheinbergon.aac.encoder.util.WAVAudioSupport;

import javax.annotation.Nonnull;
import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.spi.AudioFileWriter;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

public final class AACFileWriter extends AudioFileWriter {

  private static final int OUTPUT_BUFFER_SIZE = 16384;

  private static final int INPUT_BUFFER_MULTIPLIER = 16;

  private static final Map<AudioFileFormat.Type, AACEncodingProfile> FILE_TYPES_TO_ENCODING_PROFILES = new LinkedHashMap<>();

  static{
    FILE_TYPES_TO_ENCODING_PROFILES.put(AACFileTypes.AAC_LC, AACEncodingProfile.AAC_LC);
    FILE_TYPES_TO_ENCODING_PROFILES.put(AACFileTypes.AAC_HE, AACEncodingProfile.HE_AAC);
    FILE_TYPES_TO_ENCODING_PROFILES.put(AACFileTypes.AAC_HE_V2, AACEncodingProfile.HE_AAC_V2);
  }

  @Override
  public AudioFileFormat.Type[] getAudioFileTypes() {
    return Stream.of(AACFileTypes.AAC_LC, AACFileTypes.AAC_HE, AACFileTypes.AAC_HE_V2)
        .toArray(AudioFileFormat.Type[]::new);
  }

  @Override
  public AudioFileFormat.Type[] getAudioFileTypes(final @Nonnull AudioInputStream stream) {
    AudioFormat format = stream.getFormat();
    if (format.getEncoding().equals(AudioFormat.Encoding.PCM_SIGNED)
        && format.getSampleSizeInBits() == WAVAudioSupport.SUPPORTED_SAMPLE_SIZE
        && WAVAudioSupport.SUPPORTED_CHANNELS_RANGE.contains(format.getChannels())
        && !format.isBigEndian()) {
      return getAudioFileTypes();
    } else {
      return new AudioFileFormat.Type[0];
    }
  }

  static AACEncodingProfile profileByType(final @Nonnull AudioFileFormat.Type type) {
    return Optional.ofNullable(FILE_TYPES_TO_ENCODING_PROFILES.get(type))
        .orElseThrow(() -> new IllegalArgumentException("File type " + type + " is not yet supported"));
  }

  // Note that the bitRate is adapted automatically based on the input specification
  private static AACAudioEncoder encoder(
      final @Nonnull AudioFormat format,
      final @Nonnull AudioFileFormat.Type type) {
    return AACAudioEncoder.builder()
        .afterBurner(true)
        .channels(format.getChannels())
        .sampleRate((int) format.getSampleRate())
        .profile(profileByType(type))
        .build();
  }

  @Override
  public int write(
      final @Nonnull AudioInputStream stream,
      final @Nonnull AudioFileFormat.Type fileType,
      final @Nonnull OutputStream out) throws IOException {
    Objects.requireNonNull(stream);
    Objects.requireNonNull(fileType);
    Objects.requireNonNull(out);

    if (!isFileTypeSupported(fileType, stream)) {
      throw new IllegalArgumentException("File type " + fileType + " is not supported.");
    }
    return encodeAndWrite(stream, fileType, out);
  }

  /**
   * Tim 改动
   * @param stream
   * @param fileType
   * @param out
   * @return
   * @throws IOException
   */
  @Override
  public int write(
      final @Nonnull AudioInputStream stream,
      final @Nonnull AudioFileFormat.Type fileType,
      final @Nonnull File out) throws IOException {
    Objects.requireNonNull(stream);
    Objects.requireNonNull(fileType);
    Objects.requireNonNull(out);
    BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(out), 16384);
    BufferedOutputStream var5 = bufferedOutputStream;

    int var6;
    try {
      var6 = this.write(stream, fileType, (OutputStream)bufferedOutputStream);
    } catch (Throwable var9) {
      if (bufferedOutputStream != null) {
        try {
          var5.close();
        } catch (Throwable var8) {
          var9.addSuppressed(var8);
        }
      }

      throw var9;
    }

    if (bufferedOutputStream != null) {
      bufferedOutputStream.close();
    }

    return var6;
  }

  private int readBufferSize(
      final @Nonnull AudioFormat format,
      final @Nonnull AACAudioEncoder encoder) {
    return encoder.inputBufferSize() * INPUT_BUFFER_MULTIPLIER;
  }

  /**
   * Tim 改动
   * @param input
   * @param type
   * @param output
   * @return
   * @throws IOException
   */
  private int encodeAndWrite(
      final @Nonnull AudioInputStream input,
      final @Nonnull AudioFileFormat.Type type,
      final @Nonnull OutputStream output) throws IOException {
    boolean concluded = false;
    int encoded = 0;
    AudioFormat format = input.getFormat();
    AACAudioEncoder encoder = encoder(format, type);
    AACAudioEncoder var9 = encoder;

    try {
      int readBufferSize = this.readBufferSize(format, encoder);
      byte[] readBuffer = new byte[readBufferSize];

      while(!concluded) {
        int read = input.read(readBuffer);
        AACAudioOutput audioOutput;
        if (read == -1) {
          audioOutput = encoder.conclude();
          concluded = true;
        } else {
          WAVAudioInput audioInput = WAVAudioInput.pcms16le(readBuffer, read);
          audioOutput = encoder.encode(audioInput);
        }

        if (audioOutput.length() > 0) {
          encoded += audioOutput.length();
          output.write(audioOutput.data());
        }
      }
    } catch (Throwable var15) {
      if (encoder != null) {
        try {
          var9.close();
        } catch (Throwable var14) {
          var15.addSuppressed(var14);
        }
      }

      throw var15;
    }

    if (encoder != null) {
      encoder.close();
    }

    return encoded;
  }
}
