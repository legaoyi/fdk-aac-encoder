package org.sheinbergon.aac;


import org.sheinbergon.aac.sound.AACFileTypes;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import java.io.File;
import java.io.IOException;

public class Test {
    public static void main(String[] args) {

        try {
            AudioInputStream input = AudioSystem.getAudioInputStream(new File("D:\\Downloads\\longText4TTS.wav"));
            File output = new File("D:\\Downloads\\longText4TTS18.aac");
            AudioSystem.write(input, AACFileTypes.AAC_LC, output);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
